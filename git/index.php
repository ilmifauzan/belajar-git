<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, inital-scale=1.0">
    <title>Berlatih PHP</title>
</head>

<body>
    <?php
    $hello = "Hello world";
    echo "‹h1›$hello</h1>";
    ?>

    <?php
    // komentar baris satu
    // komentar baris dua

    /*
        komentar lebih dari satu baris
        contoh pertama
    */

    /**
     * komentar lebih dari satu baris
     * contoh kedua bisa juga seperti ini
     */
    ?>
    <?php
    // benar
        $nama1="M Abduh";
    // salah
        // $2nama = "Muh Abduh";
    // jangan lupa titik koma di setiap akhir baris kode (;)
    ?>
    <?php
  $hello ="Hello World!";
  // echo
  echo $hello;
  
  // print
  print $hello;
  
  // print_r
  print_r($hello);

  // var_dump
  var_dump($hello);

?>

<?php
  $sapa = "Halo Guys!";
  $hello = 'Hello World!';
?>

<?php
  $jargon = "Tetap Santai dan Berkualitas";
  // akan menampilkan panjang variabel $jargon yaitu 28 karakter
  echo strlen($jargon); 
  
  // akan menampilkan panjang string 11
  echo strlen("Halo semua!")
?>
<?php
$passwordasli = "belajarPHP";
$passwordinput = "belajarPHP";
$output = strcmp($passwordasli, $passwordinput);
if ($output !== 0)
  {
    echo "Password anda salah!";
  }
else
  {
    echo "Password anda benar.";
  }
?>
<?php
  $statement = "ini gak marah, cuman caps";
  
  // menampilkan INI GAK MARAH, CUMAN CAPS
  echo strtoupper($statement);
?>
<?php
  $marah = "INI MARAH";
  
  // menampilkan ini marah
  echo strtolower($marah);
?>

<?php
  $kalimat = "Saya sedang belajar PHP";
  $katayangdicari = "PHP";
  $posisi = strpos($kalimat, $katayangdicari);
  echo $posisi;
?>

<?php
$string = "Saya suka PHP";

// akan menampilkan suka PHP
echo substr($string, 5, 8);

// akan menampilkan PH
echo substr($string, -3, 2); 
?>

<?php
  
  $siswa = array("regi", "bobby", "ahmad");
  print_r($siswa);

  $trainer = ["abduh", "aghnat", "yoga"];
  print_r($trainer);

?>

<?php
$nilai = [12, 14, 19];
// akan menampilkan 12 yang merupakan elemen array indeks ke 0
echo $nilai[0];
?>

<?php
  $siswa = array("regi", "bobby", "ahmad");
  // siswa awal sebelum ditambah
  print_r($siswa);
  $siswa[] = "putri";
  // menampilkan siswa setelah ditambah data baru "putri"
  print_r($siswa);
?>

<?php
$stack = array("orange", "banana");
array_push($stack, "apple", "raspberry");
print_r($stack);
?>

<?php
$keranjang = ["pisang", "apel", "mangga"];
// menampilkan panjang array $keranjang
echo count($keranjang);
?>


<?php

$siswa1 = [ "nama" => "abduh",
            "kelas" => "laravel",
            "nilai" => 70
          ];
print_r($siswa1);

// menambahkan key value baru ke array $siswa1
$siswa1["email"] = "abduh@mail.com";

print_r($siswa1);
?>

<?php
$trainer = array (
  array("Rezky","Laravel"),
  array("Abduh","Adonis"),
  array("Iqbal","VueJs"),
);

echo "<pre>";
print_r($trainer);
echo "</pre>";
?>
</body>

</html>